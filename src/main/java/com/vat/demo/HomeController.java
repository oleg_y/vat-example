package com.vat.demo;

import com.vat.demo.models.Rate;
import com.vat.demo.models.VAT;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class HomeController {

    private static final String uri = "http://jsonvat.com/";

    @RequestMapping("/lowest")
    public List<Rate> getCountriesWithLowestStandardVAT() {
        RestTemplate restTemplate = new RestTemplate();
        VAT vat = restTemplate.getForObject(uri, VAT.class);
        return vat.getLowestStandardVATCountries();
    }

    @RequestMapping("/highest")
    public List<Rate> getCountriesWithHighestStandardVAT() {
        RestTemplate restTemplate = new RestTemplate();
        VAT vat = restTemplate.getForObject(uri, VAT.class);
        return vat.getHighestStandardVATCountries();
    }
}
