package com.vat.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Rate {
    public String name;
    public String code;
    public String country_code;
    public ArrayList<Period> periods;
}
