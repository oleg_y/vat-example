package com.vat.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
class Period {
    public Date effective_from;
    public PeriodRate rates;
}
