package com.vat.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.*;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VAT {
    public String details;
    public int version;
    public ArrayList<Rate> rates;
    private final CountryStandardRates standardRates = new CountryStandardRates();


    /**
     * Return the calculated subset of countries with lowest rates
     * @return List
     */
    public List<Rate> getLowestStandardVATCountries() {
        calculateCountryStandardRates();
        List<CountryStandardRate> rates = standardRates.getRatesSorted();
        if (rates != null) {
            return getCountries(getRatesFirstSubset(rates));
        }
        return null;
    }

    /**
     * Return the calculated subset of countries with highest rates
     * @return List
     */
    public List<Rate> getHighestStandardVATCountries() {
        calculateCountryStandardRates();
        List<CountryStandardRate> rates = standardRates.getRatesSorted();
        if (rates != null) {
            return getCountries(getRatesLastSubset(rates));
        }
        return null;
    }

    /**
     * Extract standard rates for comparison
     */
    private void calculateCountryStandardRates() {
        for (Rate rate: rates) {
            for (Period period: rate.periods) {
                standardRates.add(rate.code, period.rates.standard);
            }
        }
    }

    /**
     * Extract rates from the top of the list (lowest rates)
     * @param countryStandardRates List
     * @return List subset of rates
     */
    private List<CountryStandardRate> getRatesFirstSubset(List<CountryStandardRate> countryStandardRates) {
        int size = countryStandardRates.size();
        if (size >= 3) {
            return countryStandardRates.subList(0, 3);
        }
        return countryStandardRates;
    }

    /**
     * Extract rates from the bottom of the list (highest rates)
     * @param countryStandardRates List
     * @return List subset of rates
     */
    private List<CountryStandardRate> getRatesLastSubset(List<CountryStandardRate> countryStandardRates) {
        int size = countryStandardRates.size();
        if (size >= 3) {
            return countryStandardRates.subList(size - 3, size);
        }
        return countryStandardRates;
    }

    /**
     * Extract countries by rates for final data set
     * @param countryStandardRates List
     * @return List collection of original rates
     */
    private List<Rate> getCountries(List<CountryStandardRate> countryStandardRates) {
        ArrayList<Rate> originalRates = new ArrayList<>();
        for (CountryStandardRate standardRate: countryStandardRates) {
            Rate rate = getCountry(standardRate);
            if (rate != null) {
                originalRates.add(rate);
            }
        }
        return originalRates;
    }

    /**
     * Get the original country by rate, if found
     * @param countryRate CountryRate
     * @return Rate
     */
    private Rate getCountry(CountryStandardRate countryRate) {
        return rates.stream().filter(rate -> countryRate.getCountryCode().equals(rate.code)).findAny().orElse(null);
    }
}
