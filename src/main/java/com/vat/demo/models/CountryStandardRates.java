package com.vat.demo.models;

import java.util.*;

class CountryStandardRates {
    private ArrayList<CountryStandardRate> standardRates;
    private final HashMap<String, Integer> rates = new HashMap<>();

    void add(String code, int standard_rate) {
        if (rates.containsKey(code)) {
            increaseRate(code, standard_rate);
        } else {
            rates.put(code, standard_rate);
        }
    }

    private void increaseRate(String code, int standard_rate) {
        if (rates.get(code) < standard_rate) {
            rates.put(code, standard_rate);
        }
    }

    private void buildSortedRates() {
        standardRates = new ArrayList<>();
        for (Map.Entry<String, Integer> rate: rates.entrySet()) {
            standardRates.add(new CountryStandardRate(rate.getKey(), rate.getValue()));
        }
    }

    List<CountryStandardRate> getRatesSorted() {
        buildSortedRates();
        Collections.sort(standardRates);
        return standardRates;
    }
}
