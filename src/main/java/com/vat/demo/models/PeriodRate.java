package com.vat.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PeriodRate {
    public int super_reduced;
    public int reduced;
    public int standard;
}
