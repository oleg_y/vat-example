package com.vat.demo.models;

class CountryStandardRate implements Comparable<CountryStandardRate> {
    private final String countryCode;
    private final Integer standardRate;

    String getCountryCode() {
        return countryCode;
    }

    CountryStandardRate(String countryCode, Integer standardRate) {
        this.countryCode = countryCode;
        this.standardRate = standardRate;
    }

    @Override
    public int compareTo(CountryStandardRate rate) {
        return standardRate.compareTo(rate.standardRate);
    }
}
