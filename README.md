## Installation
_IntelliJ IDEA_
1. Clone project
2. Build, run as Spring Boot application

_Command Prompt (Windows)_
1. Clone project
2. From the project folder, run gradlew.bat build
3. Run java -jar build\libs\demo-0.0.1-SNAPSHOT.jar

_Usage_
1. Open browser at http://localhost:8080/highest to get the countries with highest standard VAT
2. Open browser at http://localhost:8080/lowest to get the countries with lowest standard VAT 